import { Component } from '@angular/core';

@Component({
  selector: 'no-content',
  template: `
    <div class="container">
      <h1>404</h1>
      <h4>:(</h4>
    </div>
  `
})
export class NoContentComponent {

}
