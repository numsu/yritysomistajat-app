import {Injectable} from '@angular/core';

@Injectable()
export class ConfigService {

    _url: string = '';
    url: string = 'http://localhost:8080/';

    constructor() {
    }

    getUrl(): string {
        return this.url;
    }

}
