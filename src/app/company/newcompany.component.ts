import {Component, OnInit} from '@angular/core';
import {CompaniesService} from "./companies.service";
import {NgForm} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
    templateUrl: 'newcompany.component.html'
})
export class NewCompanyComponent implements OnInit {

    company: ICompany;
    edit: boolean = false;

    constructor(private companyService: CompaniesService,
                private route: ActivatedRoute,
                private location: Location) {
        this.company = {};
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = +params['id'];
            if (id) {
                this.edit = true;
                this.companyService.getCompany(id).subscribe(response => {
                    this.company = response;
                })
            }
        });
    }


    saveCompany(form: NgForm): void {
        if (form.invalid) {
            return;
        }
        this.companyService.saveCompany(this.company).subscribe(() => {
            this.location.back();
        });
    }

    removeCompany(): void {
        this.company.delete = true;
        this.companyService.saveCompany(this.company).subscribe(() => {
            this.location.back();
        });
    }

}

export interface ICompany {
    id?: number;
    delete?: boolean;
    name?: string;
    identification?: string;
    land?: string;
    shares?: Array<IShare>;
}

export interface IShare {
    delete?: boolean;
    company?: ICompany;
    shareholder?: any;
    holding?: number;
    type?: number;
}
