import {Component, OnInit} from '@angular/core';
import {CompaniesService} from "./companies.service";
import {ICompany, IShare} from "./newcompany.component";
import {ActivatedRoute} from "@angular/router";

@Component({
    templateUrl: 'company.component.html'
})
export class CompanyComponent implements OnInit {

    company: ICompany = {};

    constructor(private companyService: CompaniesService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = +params['id'];
            this.getCompany(id)
        });
    }

    deleteShare(share: IShare): void {
        share.delete = true;
        this.companyService.saveShare(share).subscribe(() => {
            this.getCompany(this.company.id);
        });
    }

    getCompany(id: number): void {
        this.companyService.getCompany(id).subscribe(response => {
            this.company = response;
        })
    }

}
