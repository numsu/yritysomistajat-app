import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {ConfigService} from "../config/config.service";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {ICompany, IShare} from "./newcompany.component";

@Injectable()
export class CompaniesService {

    companyUrl: string;
    shareUrl: string;

    constructor(private http: Http,
                private configService: ConfigService) {
        this.companyUrl = this.configService.getUrl() + 'company';
        this.shareUrl = this.configService.getUrl() + 'share';
    }

    getAllCompanies(): any {
        return this.http.get(this.companyUrl + '/getall').map((res:Response) => res.json());
    }

    getCompany(id: number): any {
        return this.http.get(this.companyUrl + '/get/' + id).map((res:Response) => res.json());
    }

    saveCompany(company: ICompany): any {
        return this.http.post(this.companyUrl + '/save', company).map((res:Response) => res.json());
    }

    saveShare(share: IShare): any {
        return this.http.post(this.shareUrl + '/save', share).map((res:Response) => res.json());
    }

}
