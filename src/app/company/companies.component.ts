import {Component, OnInit} from '@angular/core';
import {CompaniesService} from "./companies.service";
import {ICompany, IShare} from "./newcompany.component";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
	templateUrl: 'companies.component.html'
})
export class CompaniesComponent implements OnInit {

	companies: Array<ICompany> = [];
	sharemode: boolean = false;
	company: ICompany;

	constructor(private companyService: CompaniesService,
				private route: ActivatedRoute,
				private router: Router) {
	}

	ngOnInit() {
		this.route.params.subscribe(params => {
			let id = +params['companyId'];
			if (id) {
				this.sharemode = true;
				this.companyService.getCompany(id).subscribe(response => {
					this.company = response;
				});
			}
		});

		this.companyService.getAllCompanies().subscribe(response => {
			this.companies = response;
		});
	}

	saveShare(company): void {
		if (!company.holding) {
			return null;
		}
		company.type = 1;
		let share: IShare = {
			company: this.company,
			shareholder: company,
			holding: company.holding
		};
		this.companyService.saveShare(share).subscribe(res => {
			this.router.navigate(['/company', this.company.id, 'view']);
		});
	}

}
