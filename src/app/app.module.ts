import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {NgModule, ApplicationRef} from '@angular/core';
import {removeNgStyles, createNewHosts, createInputTransfer} from '@angularclass/hmr';
import {RouterModule, PreloadAllModules} from '@angular/router';
import {ENV_PROVIDERS} from './environment';
import {ROUTES} from './app.routes';
import {AppComponent} from './app.component';
import {AppState, InternalStateType} from './app.service';
import {HomeComponent} from './home';
import {NoContentComponent} from './no-content';
import {CompaniesComponent} from "./company/companies.component";
import {CompaniesService} from "./company/companies.service";
import {ConfigService} from "./config/config.service";
import {NewCompanyComponent} from "./company/newcompany.component";
import {CompanyComponent} from "./company/company.component";
import {PersonComponent} from "./person/newperson.component";
import {PersonService} from "./person/person.service";
import {PersonsComponent} from "./person/persons.component";
import {LandValidator} from "./validators/land.validator";
import {DateValidator} from "./validators/date.validator";

const APP_PROVIDERS = [
    AppState,
    CompaniesService,
    PersonService,
    ConfigService
];

type StoreType = {
    state: InternalStateType,
    restoreInputValues: () => void,
    disposeOldHosts: () => void
};

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        HomeComponent,

        CompaniesComponent,
        NewCompanyComponent,
        CompanyComponent,
        PersonComponent,
        PersonsComponent,

        LandValidator,
        DateValidator,

        NoContentComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    ],
    providers: [
        ENV_PROVIDERS,
        APP_PROVIDERS,
    ]
})
export class AppModule {

    constructor(
        public appRef: ApplicationRef,
        public appState: AppState
    ) {}

    public hmrOnInit(store: StoreType) {
        if (!store || !store.state) {
            return;
        }
        this.appState._state = store.state;
        if ('restoreInputValues' in store) {
            let restoreInputValues = store.restoreInputValues;
            setTimeout(restoreInputValues);
        }

        this.appRef.tick();
        delete store.state;
        delete store.restoreInputValues;
    }

    public hmrOnDestroy(store: StoreType) {
        const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
        const state = this.appState._state;
        store.state = state;
        store.disposeOldHosts = createNewHosts(cmpLocation);
        store.restoreInputValues  = createInputTransfer();
        removeNgStyles();
    }

    public hmrAfterDestroy(store: StoreType) {
        store.disposeOldHosts();
        delete store.disposeOldHosts;
    }

}
