import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {PersonService} from "./person.service";
import {Location} from "@angular/common";
let moment = require('moment');

@Component({
    templateUrl: 'newperson.component.html'
})
export class PersonComponent implements OnInit {

    person: IPerson = {};
    edit: boolean = false;
    birthdate: string;

    constructor(private personService: PersonService,
                private route: ActivatedRoute,
                private location: Location) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = +params['id'];
            if (id) {
                this.edit = true;
                this.personService.getPerson(id).subscribe(response => {
                    this.person = response;
                    this.birthdate = moment(response.birthdate).format('DD.MM.YYYY');
                });
            }
        });
    }

    savePerson(form: NgForm): void {
        if (form.invalid) {
            return;
        }
        this.person.birthdate = moment(this.birthdate, 'DD.MM.YYYY').toDate();
        this.personService.savePerson(this.person).subscribe(() => {
            this.location.back();
        });
    }

    removePerson(): void {
        this.person.delete = true;
        this.personService.savePerson(this.person).subscribe(() => {
            this.location.back();
        });
    }

}

export interface IPerson {
    delete?: boolean;
    name?: string;
    firstname?: string;
    fullname?: string;
    identification?: string;
    birthdate?: string;
    land?: string;
    taxcode?: string;
}
