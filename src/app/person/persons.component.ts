import {Component, OnInit} from '@angular/core';
import {PersonService} from "./person.service";
import {IPerson} from "./newperson.component";
import {ActivatedRoute, Router} from "@angular/router";
import {ICompany, IShare} from "../company/newcompany.component";
import {CompaniesService} from "../company/companies.service";

@Component({
    templateUrl: 'persons.component.html'
})
export class PersonsComponent implements OnInit {

    persons: Array<IPerson> = [];
    sharemode: boolean = false;
    company: ICompany;
    share: IShare;

    constructor(private personService: PersonService,
                private route: ActivatedRoute,
                private companyService: CompaniesService,
                private router: Router) {
    }

    ngOnInit() {
        this.route.params.subscribe(params => {
            let id = +params['companyId'];
            if (id) {
                this.sharemode = true;
                this.companyService.getCompany(id).subscribe(response => {
                    this.company = response;
                });
            }
        });
        this.personService.getAllPersons().subscribe(response => {
            this.persons = response;
        });
    }

    saveShare(person): void {
        if (!person.holding) {
            return null;
        }
        person.type = 2;
        let share: IShare = {
            company: this.company,
            shareholder: person,
            holding: person.holding
        };
        this.companyService.saveShare(share).subscribe(res => {
            this.router.navigate(['/company', this.company.id, 'view']);
        });
    }

}
