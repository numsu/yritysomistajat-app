import {Injectable} from '@angular/core';
import {Http, Response} from "@angular/http";
import {ConfigService} from "../config/config.service";
import {IPerson} from "./newperson.component";
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable()
export class PersonService {

    personUrl: string;

    constructor(private http: Http,
                private configService: ConfigService) {
        this.personUrl = this.configService.getUrl() + 'person';
    }

    getAllPersons(): any {
        return this.http.get(this.personUrl + '/getall').map((res:Response) => res.json());
    }

    getPerson(id: number): any {
        return this.http.get(this.personUrl + '/get/' + id).map((res:Response) => res.json());
    }

    savePerson(person: IPerson): any {
        return this.http.post(this.personUrl + '/save', person).map((res:Response) => res.json());
    }


}
