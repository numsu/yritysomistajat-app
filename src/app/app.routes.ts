import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { NoContentComponent } from './no-content';
import {CompaniesComponent} from "./company/companies.component";
import {NewCompanyComponent} from "./company/newcompany.component";
import {CompanyComponent} from "./company/company.component";
import {PersonComponent} from "./person/newperson.component";
import {PersonsComponent} from "./person/persons.component";

export const ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'companies', component: CompaniesComponent },
  { path: 'persons', component: PersonsComponent },
  { path: 'company/new', component: NewCompanyComponent },
  { path: 'company/:id/edit', component: NewCompanyComponent },
  { path: 'company/:id/view', component: CompanyComponent },
  { path: 'person/new', component: PersonComponent },
  { path: 'person/:id/edit', component: PersonComponent },
  { path: 'company/:companyId/share/company', component: CompaniesComponent },
  { path: 'company/:companyId/share/person', component: PersonsComponent },
  { path: '**', component: NoContentComponent },
];
