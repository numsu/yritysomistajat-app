import {Directive, forwardRef} from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';
let moment = require('moment');

function validateDateFactory() {
    return (c: FormControl) => {
        let valid = true;
        let value: string = c.value;

        if (!value) {
            return null;
        }

        if (value.length != 10) {
            valid = false;
        }

        let date = moment(value, 'DD.MM.YYYY');

        if (!date.isValid()) {
            valid = false;
        }

        return valid ? null : {
                validateDate: {
                    valid: false
                }
            };
    };
}

@Directive({
    selector: '[validateDate][ngModel],[validateDate][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => DateValidator), multi: true }
    ]
})
export class DateValidator {
    validator: Function;

    constructor() {
        this.validator = validateDateFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}