import { Directive, forwardRef } from '@angular/core';
import { NG_VALIDATORS, FormControl } from '@angular/forms';

function validateLandFactory() {
    return (c: FormControl) => {
        let valid = true;
        let value: string = c.value;

        if (!value) {
            return null;
        }

        if (value.length == 0 || value.length > 2) {
            valid = false;
        }

        // If string contains something else than characters
        if (!/^[A-Za-z]+$/.test(value)) {
            valid = false;
        }

        return valid ? null : {
                validateLand: {
                    valid: false
                }
            };
    };
}

@Directive({
    selector: '[validateLand][ngModel],[validateLand][formControl]',
    providers: [
        { provide: NG_VALIDATORS, useExisting: forwardRef(() => LandValidator), multi: true }
    ]
})
export class LandValidator {

    validator: Function;

    constructor() {
        this.validator = validateLandFactory();
    }

    validate(c: FormControl) {
        return this.validator(c);
    }
}