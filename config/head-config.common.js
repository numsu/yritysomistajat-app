module.exports = {
  link: [
    { rel: 'icon', type: 'image/png', sizes: '32x32', href: '/assets/icon/favicon-32x32.png' },
    { rel: 'manifest', href: '/assets/manifest.json' }
  ]
};
